import re

import networkx as nx
import matplotlib.pyplot as plt

line_regex = r'Card +(\d*): (.*) \| (.*)'
total = 0
with open('./day4_test.txt') as file:
    for line in file:
        winning_numbers = [int(i) for i in re.search(line_regex, line).group(2).split()]
        ticket_numbers = [int(i) for i in re.search(line_regex, line).group(3).split()]

        matching_numbers = list(set(winning_numbers) & set(ticket_numbers))
        
        if len(matching_numbers) != 0:
            num_matching = len(matching_numbers)
            total += pow(2,num_matching)
    print(int(total/2))

connections = []
with open('./day4_test.txt') as file:
    for line in file:
        winning_numbers = [int(i) for i in re.search(line_regex, line).group(2).split()]
        ticket_numbers = [int(i) for i in re.search(line_regex, line).group(3).split()]

        current_ticket = int(re.search(line_regex, line).group(1))
        matching_numbers = list(set(winning_numbers) & set(ticket_numbers))

        if len(matching_numbers) != 0: 
            for i in range(1,len(matching_numbers)+1):
                connections.append((current_ticket,current_ticket+i))
    
    G = nx.DiGraph()
    G.add_edges_from(connections)
    subax1 = plt.subplot(121)
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.show()
    print(connections)
    